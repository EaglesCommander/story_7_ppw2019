$(() =>{

    var panels = $('.accordion > dd').hide();
    $('body').css('background-color', 'white');

    var hrefs = $('.accordion > dt').click(function() {

        var $this = $(this);

        panels.slideUp();

        if ($this.next().is(":hidden")){
            $this.next().slideDown();
        }

        return false;
    });
});

function changeTheme(){
    var body = $('body');
    var unordered_list = $('ul');
    var button = $('button')

    if (button.attr('id') == "0"){
        body.css("background-color", "gray");
        body.css("font-family", "Impact");

        unordered_list.css("list-style-type", "square");

        button.attr('id', "1");
    }
    else {
        body.css("background-color", "white");
        body.css("font-family", "Arial");

        unordered_list.css("list-style-type", "circle");

        button.attr('id', "0");
    }
    return false;
}

