from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from .views import index

class HomepageFunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        options = Options()
        options.headless = True
        self.selenium = webdriver.Firefox(options=options)
        self.selenium.implicitly_wait(3)
        super(HomepageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(HomepageFunctionalTest, self).tearDown()

    def test_page_works_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        self.assertIn('About Me', self.selenium.page_source)
    
    def test_accordion_works_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        dd = selenium.find_elements_by_id('accordion-item')
        self.assertEqual(dd[0].value_of_css_property('display'), "none")

        dt = selenium.find_elements_by_id('accordion-button')
        dt[0].click()
        time.sleep(2)
        self.assertEqual(dd[0].value_of_css_property('display'), "block")

        dt[1].click()
        time.sleep(2)
        self.assertEqual(dd[0].value_of_css_property('display'), "none")
        self.assertEqual(dd[1].value_of_css_property('display'), "block")

        dt[1].click()
        time.sleep(2)
        self.assertEqual(dd[0].value_of_css_property('display'), "none")
        self.assertEqual(dd[1].value_of_css_property('display'), "none")
        self.assertEqual(dd[2].value_of_css_property('display'), "none")

    def test_theme_button_works_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        button = selenium.find_element_by_tag_name('button')
        body = selenium.find_element_by_tag_name('body')

        self.assertEqual(body.value_of_css_property('background-color'), "rgb(255, 255, 255)")

        button.click()
        time.sleep(2)

        self.assertEqual(body.value_of_css_property('background-color'), "rgb(128, 128, 128)")


class HomepageUnitTest(TestCase):

    def test_homepage_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
        
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)